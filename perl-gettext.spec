Name:            perl-gettext
Version:         1.07
Release:         16
Summary:         message handling functions
License:         GPL-1.0-or-later OR Artistic-1.0-Perl
URL:             https://metacpan.org/release/gettext
Source0:         https://cpan.metacpan.org/authors/id/P/PV/PVANDRY/Locale-gettext-%{version}.tar.gz

BuildRequires:   perl-interpreter perl-generators perl-devel gcc
BuildRequires:   perl(ExtUtils::MakeMaker) perl(Test::More)

%description
The gettext module permits access from perl to the gettext() family of functions
for retrieving message strings from databases constructed to internationalize
software.

%package -n perl-Locale-gettext
Summary:         message handling functions

%description -n perl-Locale-gettext
The gettext module permits access from perl to the gettext() family of functions
for retrieving message strings from databases constructed to internationalize
software.

%package_help

%prep
%autosetup -n Locale-gettext-%{version} -p1

%build
perl Makefile.PL NO_PACKLIST=1 INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files -n perl-Locale-gettext
%doc README
%{perl_vendorarch}/auto/Locale
%{perl_vendorarch}/Locale

%files help
%{_mandir}/man3/*3*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 1.07-16
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jun 15 2021 wulei <wulei80@huawei.com> - 1.07-15
- fixes failed: add buildrequire gcc

* Wed May 13 2020 licunlong <licunlong1@huawei.com> - 1.07-14
- add perl-devel buildrequire

* Mon Dec 9 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.07-13
- Package init
